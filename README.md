# Software Studio 2018 Spring Lab03 Bootstrap and RWD
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. **Fork this repo to your account, remove fork relationship and change project visibility to public**
1. Make a personal webpage that has RWD
2. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
3. Use any template form the inernet but you should provide the URL in your project README.md
4. Use at least **Ten** bootstrap elements by yourself which is not include the template
5. You web page template should totally fit to your original personal web page content
6. Modify your markdown properly (check all todo and fill all item)
7. New Merge Request
8. Go home!

## Student ID , Name and Template URL
- [ ] Student ID : 
- [ ] Name :
- [ ] URL :